<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('think', function () {
    return 'hello,ThinkPHP5!';
});

Route::get('hello/:name', 'index/hello');

return [
    'index' => 'home/index/index',

    'news/:catId' => 'home/news/index',
    'newsInfo/:id/:catId' => 'home/news/info',

    'about/:catId' => 'home/about/index',

    'system/:catId' => 'home/system/index',

    'services/:catId' => 'home/services/index',
    'servicesInfo/:id/:catId' => 'home/services/info',

    'team/:catId' => 'home/team/index',
    'contact/:catId' => 'home/contact/index',
];
